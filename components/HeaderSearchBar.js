import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FontisoIcon from 'react-native-vector-icons/Fontisto';

const HeaderSearchBar = () => {
  return (
    <View style={styles.container}>
      <FontAwesomeIcon name="paper-plane" size={25} color="white" />
      <View style={styles.searchBox}>
        <FontAwesomeIcon name="search" size={20} color="white" />
        <TextInput
          style={styles.searchBar}
          placeholder="Search"
          placeholderTextColor="grey"
        />
      </View>
      <FontisoIcon name="messenger" size={25} color="white" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3B5998',
    height: hp('9%'),
    width: wp('100%'),
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingBottom: hp('1%'),
    paddingTop: hp('1%'),
  },
  searchBox: {
    // backgroundColor: 'salmon',
    flexDirection: 'row',
    width: wp('70%'),
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  searchBar: {
    // backgroundColor: 'wheat',
    flex: 1,
    paddingTop: hp('1%'),
    paddingBottom: hp('1%'),
    paddingLeft: wp('3%'),
    fontSize: 20,
  },
});

export default HeaderSearchBar;
