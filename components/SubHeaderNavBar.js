import React from 'react';
import {StyleSheet, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

const SubHeaderNavBar = () => {
  return (
    <View style={styles.container}>
      <EntypoIcon name="browser" size={25} style={styles.activeIconBox} />
      <EntypoIcon name="users" size={25} style={styles.iconBox} />
      <AntDesignIcon name="play" size={25} style={styles.iconBox} />
      <FontAwesomeIcon name="globe" size={25} style={styles.iconBox} />
      <EntypoIcon name="menu" size={25} style={styles.iconBox} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: hp('9%'),
    width: wp('100%'),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    elevation: 4,
  },
  searchBar: {
    width: wp('70%'),
  },
  activeIconBox: {
    color: '#3B5998',
  },
  iconBox: {
    color: 'lightgrey',
  },
});

export default SubHeaderNavBar;
