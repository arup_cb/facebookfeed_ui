import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import GobeyondImage from '../images/gobeyond.jpg';
import Flipkart from '../images/flipkart.jpeg';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const InfoContainer = () => {
  return (
    <View style={styles.container}>
      <View style={styles.headerBox}>
        <View style={styles.companyPageImageBox}>
          <Image source={Flipkart} style={styles.companyPageImage} />
        </View>
        <View style={styles.companyPageContent}>
          <View style={styles.dropdownSection}>
            <Text style={styles.dropdownText}>Flipkart</Text>
            <MaterialIcons name="keyboard-arrow-down" size={30} color="grey" />
          </View>
          <View style={styles.headerContent}>
            <Text style={styles.headerContentText}>Sponsored</Text>
            <Text style={styles.headerContentText}>&middot;</Text>
            <FontAwesomeIcon
              name="globe"
              size={15}
              style={styles.iconBox}
              color="grey"
            />
          </View>
        </View>
      </View>
      <Text style={styles.subheaderText}>
        Guess what's making news on Flipkart!
      </Text>
      <View style={styles.imageContainer}>
        <View style={styles.imageBox}>
          <Image source={GobeyondImage} style={styles.image} />
        </View>
        <View style={styles.imageContent}>
          <View style={styles.description}>
            <Text style={styles.descTopText}>Are you ready to #GoBeyond?</Text>
            <Text style={styles.descMiddleText}>Coming soon...</Text>
            <Text style={styles.descBottomText}>www.flipkart.com</Text>
          </View>
          <TouchableOpacity style={styles.btnBox}>
            <Text style={styles.btnText}>LEARN MORE</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.commentBox}>
        <View style={styles.topSection}>
          <View style={styles.emojiSection}>
            <MaterialIcons
              name="thumb-up"
              size={10}
              color="white"
              style={styles.likeIconBox}
            />
            <IoniconsIcon
              name="md-heart-circle"
              size={25}
              color="red"
              style={styles.heartIconBox}
            />
            <MaterialCommunityIcons
              name="emoticon-excited"
              size={25}
              color="#F8E16C"
              style={styles.smileyIcon}
            />
            <Text style={styles.numberText}>2.7k</Text>
          </View>
          <Text style={styles.shareAandCommentText}>
            77 Comments &middot; 13 Shares
          </Text>
        </View>
        <View style={styles.bottomSection}>
          <View style={styles.interactSectionBox}>
            <AntDesignIcon name="like1" size={20} color="grey" />
            <Text style={styles.likeText}>Like</Text>
          </View>
          <View style={styles.interactSectionBox}>
            <MaterialCommunityIcons name="comment" size={20} color="grey" />
            <Text style={styles.commentText}>Comment</Text>
          </View>
          <View style={styles.interactSectionBox}>
            <FontistoIcon name="share-a" size={20} color="grey" />
            <Text style={styles.shareText}>Share</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: hp('70%'),
    width: wp('100%'),
    marginTop: hp('1.5%'),
    alignItems: 'center',
  },
  headerBox: {
    height: hp('10%'),
    width: wp('95%'),
    flexDirection: 'row',
    alignItems: 'center',
  },
  companyPageImageBox: {
    height: wp('15%'),
    width: wp('15%'),
  },
  companyPageImage: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
  },
  companyPageContent: {
    flex: 1,
    paddingLeft: wp(2),
  },
  dropdownSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dropdownText: {
    fontSize: 18,
    fontWeight: '700',
  },
  headerContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerContentText: {
    color: 'grey',
    paddingRight: wp(1),
    fontSize: 15,
  },
  subheaderText: {
    width: wp('95%'),
    fontSize: 18,
    paddingTop: hp('1%'),
    paddingBottom: hp('1.5%'),
  },
  imageContainer: {
    backgroundColor: 'white',
    width: wp('95%'),
    height: hp('45%'),
    elevation: 2,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
  },
  imageBox: {
    width: wp('95%'),
    height: wp('50%'),
  },
  image: {
    width: '100%',
    height: '100%',
  },
  imageContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: wp('3%'),
  },
  description: {
    // backgroundColor: 'lightgreen',
  },
  descTopText: {
    fontSize: 17,
    fontWeight: '700',
  },
  descMiddleText: {
    fontSize: 16,
  },
  descBottomText: {
    fontSize: 16,
    color: 'grey',
  },
  btnBox: {
    backgroundColor: 'white',
    paddingHorizontal: wp('3%'),
    paddingVertical: wp('2%'),
    borderRadius: 4,
    borderWidth: 2,
    borderColor: 'lightgrey',
  },
  btnText: {
    color: 'grey',
    fontSize: 16,
  },
  commentBox: {
    backgroundColor: 'white',
    height: hp('15%'),
    width: wp('95%'),
  },
  topSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  emojiSection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  likeIconBox: {
    backgroundColor: '#3B5998',
    borderRadius: 30,
    borderWidth: 3,
    borderColor: 'white',
    position: 'absolute',
    zIndex: 2,
    paddingTop: hp('1%'),
    paddingBottom: hp('0.5%'),
    paddingLeft: wp('2%'),
    paddingRight: wp('0.5%'),
  },
  heartIconBox: {
    position: 'relative',
    left: 22,
    zIndex: 1,
    backgroundColor: 'white',
    borderRadius: 30,
  },
  smileyIcon: {
    position: 'relative',
    left: 18,
    backgroundColor: 'white',
    borderRadius: 20,
  },
  numberText: {
    fontSize: 16,
    color: 'grey',
    paddingLeft: wp(5),
  },
  shareAandCommentText: {
    fontSize: 16,
    color: 'grey',
    paddingLeft: wp(1),
  },
  bottomSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopWidth: 1,
    borderColor: 'lightgrey',
  },
  interactSectionBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  likeText: {
    fontSize: 16,
    color: 'grey',
    paddingLeft: wp(1),
    fontWeight: '700',
  },
  commentText: {
    fontSize: 16,
    color: 'grey',
    paddingLeft: wp(1),
    fontWeight: '700',
  },
  shareText: {
    fontSize: 16,
    color: 'grey',
    paddingLeft: wp(1),
    fontWeight: '700',
  },
});

export default InfoContainer;
