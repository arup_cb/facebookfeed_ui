import React from 'react';
import {StyleSheet, View} from 'react-native';

import HeaderSearchBar from '../components/HeaderSearchBar';
import SubHeaderNavBar from '../components/SubHeaderNavBar';
import InfoContainer from '../components/InfoContainer';
const Main = () => {
  return (
    <View style={styles.container}>
      <HeaderSearchBar />
      <SubHeaderNavBar />
      <InfoContainer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'lightgrey',
    flex: 1,
  },
});

export default Main;
